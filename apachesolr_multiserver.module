<?php

define ('ASM_PREFIX','asm' );

/**
 * @file
 * Drupal Apache Solr Multicore extension. Provides introspection, reload, unload, swapping and aliasing of solr cores
 * @author Dave Stuart (dave@axistwelve.com)
 * @author Ravi Julapalli (rkjulapalli@gmail.com)
 */

//require_once(drupal_get_path('module', 'apachesolr_multiserver') .'/apachesolr_multiserver.admin.inc');

/**
 * Implementation of hook_enale
 */
function apachesolr_multiserver_enable() {
  _apachesolr_multiserver_initalize();
  drupal_set_message(t('Apachesolr multiserver has been enabled. Click !url to add new server ', array('!url' => l('here','admin/settings/apachesolr/multiserver/add', array('absolute' => TRUE)))));
}

/**
 * Implementation of hook_help().
 */
function apachesolr_multiserver_help($path, $arg) {
  switch ($path) {
    case 'admin/help#apachesolr_multicore':
      return '<p>'. t('Drupal interface to Apache Solr <ulticore. Provides introspection, reload, unload, swapping and aliasing of solr cores.') .'</p>';
    case 'admin/modules#description':
      return t('Interface to Apache Solr Multicore.');
  }
}

/**
 * implementation of hook_perm
 * Enter description here ...
 */
function apachesolr_multiserver_perm() {
  return array('administer apachesolr multiserver');
}

function apachesolr_multiserver_menu() {
  $items = array();
  $items['admin/settings/apachesolr/multiserver'] = array(
      'title'              => 'Apache Solr Servers',
      'page callback'      => 'apachesolr_multiserver_list',
      'access arguments'   => array('administer apachesolr multiserver'),
      'description'        => 'List of Available Solr Servers',
      'file'               => 'apachesolr_multiserver.admin.inc',
      'type'               => MENU_LOCAL_TASK,
  );
  $items['admin/settings/apachesolr/multiserver/list'] = array(
      'title'              => 'List Servers',
      'access arguments'   => array('administer apachesolr multiserver'),
      'type'               => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['admin/settings/apachesolr/multiserver/add'] = array(
      'title'              => 'Add Server',
      'page callback'      => 'drupal_get_form',
      'page arguments'     => array('apachesolr_multiserver_server_form', 'add'),
      'access arguments'   => array('administer apachesolr multiserver'),
      'description'        => 'Add a new solr server',
      'file'               => 'apachesolr_multiserver.admin.inc',
      'type'               => MENU_LOCAL_TASK,
  );
  $items['admin/settings/apachesolr/multiserver/%/edit'] = array(
      'page callback'      => 'drupal_get_form',
      'page arguments'     => array('apachesolr_multiserver_server_form', 'edit', 4),
      'access arguments'   => array('administer apachesolr multiserver'),
      'description'        => 'Edit an existing server',
      'file'               => 'apachesolr_multiserver.admin.inc',
      'type'               => MENU_CALLBACK,
  );
  $items['admin/settings/apachesolr/multiserver/%/delete'] = array(
      'page callback'      => 'drupal_get_form',
      'page arguments'     => array('apachesolr_multiserver_server_delete', 4),
      'access arguments'   => array('administer apachesolr multiserver'),
      'description'        => 'Delete an existing server',
      'file'               => 'apachesolr_multiserver.admin.inc',
      'type'               => MENU_CALLBACK,
  );
  $items['admin/settings/apachesolr/multiserver/core/reload'] = array(
      'title'              => 'Apache Solr Reload Core',
      'page callback'      => 'apachesolr_multiserver_reload_core',
      'page arguments'     => array(6, 7),
      'access arguments'   => array('administer apachesolr multiserver'),
      'description'        => 'Reload a specific core',
      'file'               => 'apachesolr_multiserver.admin.inc',
      'type'               => MENU_CALLBACK,
  );
  $items['admin/settings/apachesolr/multiserver/dih/full'] = array(
      'title'              => 'Apache Solr Full Import',
      'page callback'      => 'apachesolr_multiserver_dih_full_import',
      'page arguments'     => array(6, 7),
      'access arguments'   => array('administer apachesolr multiserver'),
      'description'        => 'Full import using DIH',
      'file'               => 'apachesolr_multiserver.admin.inc',
      'type'               => MENU_CALLBACK,
  );
  $items['admin/settings/apachesolr/multicore/ahah'] = array(
      'page callback'      => 'apachesolr_multiserver_ahah',
      'access arguments'   => array('administer apachesolr multiserver'),
      'description'        => 'Reload a specific core',
      'file'               => 'apachesolr_multiserver.admin.inc',
      'type'               => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Preprocess function for search results
 * Enter description here ...
 * @param unknown_type $variables
 */
function apachesolr_multiserver_preprocess_search_result(&$variables) {
   $variables["info"] .= " - ". l($variables["result"]["node"]->url, $variables["result"]["node"]->url);
  // ' - <a href="' . $variables['result']['node']->url . '" >' . $variables['result']['node']->url . '</a>' ;
  }


  /**
   * Implementation of hook_form_alter
   * Adds additional form elements allowing users to choose one of the registered Apachesolr servers and the core
   * @param $form
   * @param $form_state
   * @param $form_id
   */
  function apachesolr_multiserver_form_alter(&$form, $form_state, $form_id) {
    module_load_include('inc','apachesolr_multiserver','apachesolr_multiserver.admin');
    if ($form_id  == 'apachesolr_settings') {
      if ($solr_servers = apachesolr_multiserver_get_server()) {
        $form['mutlicore_fieldset'] = array(
          '#type' => 'fieldset',
          '#title' => t('Solr Multi Server'),
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
          '#weight' => -1,
        );
        $ss = array('0' => 'Manual');

        foreach ($solr_servers as $server) {
          $ss[$server->sid] = $server->apachesolr_title;
        }

        $form['mutlicore_fieldset']['markup'] = array(
          '#type' => 'markup',
          '#value' => t("!url To add a new apachesolr server", array("!url" => l("Click here", 'admin/settings/apachesolr/multiserver/add'))),
          '#weight' => -1,
        );

        $form['mutlicore_fieldset']['apachesolr_server_sid'] = array(
          '#type' => 'select',
          '#title' => t('Available Servers'),
          '#options' => $ss,
          '#default_value' => variable_get('apachesolr_server_sid', 0),
          '#description' => t('A list of available of servers'),
          '#weight' => 0,
          '#ahah' => array(
            'path' => 'admin/settings/apachesolr/multicore/ahah/',
            'wrapper' => 'core-pathes',
            'method' => 'replace',
            'event' => 'change',
        )
        );

        // We  unset the default apachesolr submit handler and add in apachesolr_multiserver submit handler
        if (isset($form['apachesolr_host'])) unset($form['apachesolr_host']);
        if (isset($form['apachesolr_port'])) unset($form['apachesolr_port']);
        if (isset($form['apachesolr_path'])) unset($form['apachesolr_path']);
        if (isset($form['#validate'])) unset($form['#validate']);

        $form['#submit'] = array();
        if(variable_get('apachesolr_server_sid', 0) != 0){
          $solr_server = apachesolr_multiserver_get_server(variable_get('apachesolr_server_sid', 0));
          if ($solr_server[0]->apachesolr_is_multicore) {
            $cores = apachesolr_multiserver_get_cores($solr_server[0]->apachesolr_host, $solr_server[0]->apachesolr_port, $solr_server[0]->apachesolr_path);
          }
          else{
            # This Server isnt multicore lets just give them the default core
            $cores = array($solr_server[0]->apachesolr_path);
          }
        }

        $options = array('Select');
        foreach($cores as $core){
          $options[$core] = $core;
        }

        $form['mutlicore_fieldset']['apachesolr_path_default'] = array(
          '#prefix' => '<div class="clear-block" id="core-pathes">',
          '#type' => 'select',
          '#id' => 'edit-apachesolr-default-path',
          '#name' => 'apachesolr_path_default',
          '#title' => t('Solr Path'),
          '#options' => $options,
          '#default_value' => variable_get('apachesolr_path_default', 0),
          '#description' => t('A list of solr paths for the chosen server'),
          '#weight' => 0,
          '#suffix' => '</div>',
        );
      }

      $form['#validate'][] = 'apachesolr_multiserver_settings_validate';
      $form['#submit'][] = 'apachesolr_multiserver_settings_override';
    }
  }

/**
 * Implementation of hook_views_api().
 */
function apachesolr_multiserver_views_api() {
  return array(
    'api' => 2,
    'path' => drupal_get_path('module', 'apachesolr_multiserver'),
  );
}

/**
 * Implemnentation of hook_apachesolr_modify_query().
 */
function apachesolr_multiserver_apachesolr_modify_query(&$query, &$params, $caller) {
  switch ($caller) {

    case 'apachesolr_views_query':
      if (variable_get('apachesolr_multicore', 0)) {
        $host = variable_get('apachesolr_host', 'localhost');
        $port = variable_get('apachesolr_port', '8983');
        $view = views_get_current_view();

        if (isset ($view->display['default']->display_options['apachesolr_multicore'])) {
          $path = $view->display['default']->display_options['apachesolr_multicore'];
          $url = url("$host:$port$path/select", array('absolute' => TRUE));
          $query->set_searchUrl($url);
        }
      }
      break;
  }
}


/**
 * Get server object(s)
 * Enter description here ...
 * @param $sid: Server id, if serverid not provided returns list of all servers registerd in the system
 */
function apachesolr_multiserver_get_server($sid=NULL) {
  $rows = array();
  $query = 'SELECT sid, apachesolr_title, apachesolr_host, apachesolr_port, apachesolr_path, apachesolr_is_multicore FROM {apachesolr_server} ';
  if (!is_null($sid)) {
    $result = db_query($query . ' where sid = %s', $sid);
  }
  else{
    $result = db_query($query);
  }
  while ($row = db_fetch_object($result)) {
    $rows[] = $row;
  }
  return $rows;
}


/**
 * Validator function for multiserver form
 * Enter description here ...
 * @param $form
 * @param $form_state
 */
function apachesolr_multiserver_settings_validate($form, &$form_state) {
  //The only validation we do is to check if user has selected a solr server from the list
  if (!isset($form_state['values']['apachesolr_server_sid']) || !ctype_digit($form_state['values']['apachesolr_server_sid'])) form_set_error('apachesolr_server_sid', 'Select server from the list');
}

/**
 *
 * Dirty little because I cant be bothereed making everything into a ahah call
 * @param $form
 * @param $form_state
 */
function apachesolr_multiserver_settings_override($form, &$form_state) {
  variable_set('apachesolr_server_sid', $form_state['values']['apachesolr_server_sid']);
  variable_set('apachesolr_path_default', $form_state['values']['apachesolr_path_default']);
}


/**
 * Is executed when module is enabled for first time.
 * Enter description here ...
 */
function  _apachesolr_multiserver_initalize() {
  // What we are doing here is to check if the current site already has apachesolr search module installed and configured, if yes we take those as default server (1)
  $server = array(
      'apachesolr_title' =>  variable_get("apachesolr_host", FALSE),
      'apachesolr_host' => variable_get("apachesolr_host", FALSE),
      'apachesolr_port' => variable_get("apachesolr_port", FALSE),
      'apachesolr_path' => variable_get("apachesolr_path", FALSE),
  );

  if (isset($server['apachesolr_host']) && isset($server['apachesolr_port']) && isset($server['apachesolr_path'])) {
    // drupal_write_record('apachesolr_server', $server);
    // Can't seem to be able to use drupal_write_record, Is cache not enabled when hook_enable is called ?
    $sql = "INSERT INTO {apachesolr_server} (`apachesolr_title`,`apachesolr_host`,`apachesolr_port`,`apachesolr_path`) values ('%s', '%s', %d, '%s')";
    db_query($sql, $server['apachesolr_title'], $server['apachesolr_host'], $server['apachesolr_port'], $server['apachesolr_path']);
    $sid = db_last_insert_id('apachesolr_server', 'sid');
    // Make the local server default
    if (isset($sid)) variable_set('apachesolr_server_sid', $sid);
    drupal_set_message(t("Found existing apachesolr configuration settings. Current configurations have been saved to  !url ", array("!url" => l('server list', "admin/settings/apachesolr/multiserver/"))));
  }
}


/**
 * Is given server id the default server being used
 */
function apachesolr_multiserver_is_default_server($sid) {
  return (variable_get('apachesolr_path_default', FALSE) == $sid) ? TRUE:FALSE;
}