OVERVIEW
--------

Apachesolr multicore module extends the traditional Apachesolr site search module by providing the ability to add multiple Apache solr servers.
Each of the servers could have more than one core effectively.  

Key features of this module include:

* Ability to register more than one apachesolr servers

* Selecting any of the cores from multicore server or manually configuring a default server.

* Integration with views, Each of the cores will show up as seperate option in first step of creating views.
* Views integration needs views3, views2 backport is not available at the moment.

USE CASE
--------

The Original usecase for this module came with a requirement where we had to syndicate content from another 
site and wanted the content to show up within a block on the site nodes. We further wanted a way to be able to
search through the syndicated content and only display nodes that have appropriate tags as that of the node
the block is displayed on. 

We addressed this requirement by deploying a Apache Solr multicore server with dedicated cores for Drupal and 
syndicated sites.

Apachesolr multicore module with views integration providing ability to create views of content from syndicated sites
Views filters to surface appropriate content based on current page tags.


INSTALL
-------

Deploy Apachesolr multicore servers.
Enable ApacheSolr Multicore module
Configure default core for the site.


USAGE
-----

Log in as an administrator. (uid==1, or a user with
administer Multicore permission)

* Goto adminsiter -> site configuration -> Apachesolr settings (/admin/settings/apachesolr_settings)
* Click on Apache Solr Server List tab
* Click on Add server tab to add new server
* Click on Apache Solr Server list to manage your servers
* Goto Settings tab, you will see drop down with list of servers you have added. Select the Apachesolr server for your default server.
* Select the core for default search.

AUTHOR
------
< TO BE FILLED>