<?php

/**
 * @file Setting file for apachesolr
 * The admin functionality of this module will implement the following
 * @author Dave Stuart (dave@axistwelve.com)
 * @author Ravi Julapalli (rkjulapalli@gmail.com)
 *
 * Background to the functions
 *
 * Implemented:
 * RELOAD: Load a new core from the same configuration as an existing registered core.
 * 		   While the "new" core is initalizing, the "old" one will continue to accept requests.
 *         Once it has finished, all new request will go to the "new" core, and the "old" core will be unloaded.
 * 	       This can be useful when (backwards compatible) changes have been made to your solrconfig.xml or schema.xml
 *         files (ie: new <field> declarations, changed default params for a <requestHandler>, etc...) and you want to start
 *         using them without stopping and restarting your whole Servlet Container.
 *
 * DIH FULL Import:
 *
 * To be Implemented:
 * STATUS: Get the status for a given core or all cores if no core is specified:
 * 			e.g. http://localhost:8983/solr/admin/cores?action=STATUS&core=core0
 * 			http://localhost:8983/solr/admin/cores?action=STATUS
 *
 * CREATE: Creates a new core based on preexisting instanceDir/solrconfig.xml/schema.xml, and registers it.
 * 			If persistence is enabled (persist=true), the configuration for this new core will be saved in 'solr.xml'.
 * 			If a core with the same name exists, while the "new" created core is initalizing, the "old" one will continue to accept requests.
 * 			Once it has finished, all new request will go to the "new" core, and the "old" core will be unloaded.
 * 			e.g. http://localhost:8983/solr/admin/cores?action=CREATE&name=coreX&instanceDir=path_to_instance_directory&config=config_file_name.xml&schema=schem_file_name.xml&dataDir=data
 * 			instanceDir is a required parameter. config, schema & dataDir parameters are optional.
 * 			(Default is to look for solrconfig.xml/schema.xml inside instanceDir. Default place to look for dataDir depends on solrconfig.xml.)
 *
 * RENAME: Change the names used to access a core. The example below changes the name of the core from "core0" to "core5".
 * 			e.g. http://localhost:8983/solr/admin/cores?action=RENAME&core=core0&other=core5
 *
 * ALIAS: (Experimental) Adds an additional name for a core. The example below allows access to the same core via the names "core0"
 * 			and "corefoo".
 * 			e.g. http://localhost:8983/solr/admin/cores?action=ALIAS&core=core0&other=corefoo
 *
 * SWAP: Atomically swaps the names used to access two existing cores.
 * 		This can be useful for replacing a "live" core with an "ondeck" core, and keeping the old "live" core running in case you
 * 		decide to roll-back.
 * 		e.g. http://localhost:8983/solr/admin/cores?action=SWAP&core=core1&other=core0
 *
 * UNLOAD: Removes a core from solr. Existing requests will continue to be processed, but no new requests can be sent to this core by the name.
 * 			If a core is registered under more than one name, only that specific mapping is removed.
 * 			e.g. http://localhost:8983/solr/admin/cores?action=UNLOAD&core=core0
 */
function apachesolr_multicore_admin_settings() {

  $form = array();
  $form['system'] = array(
    '#type' => 'fieldset',
    '#title' => t('System settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  return system_settings_form($form);
}
############## Core Manipulation Functions

/**
 *
 * Allow you to reload the configuration in an existing core
 * @param $core Name of the core
 */
function apachesolr_multiserver_reload_core($core, $sid=NULL) {
  if (is_null($sid)) {
   $host = variable_get('apachesolr_host', 'localhost');
   $port = variable_get('apachesolr_port', '8983');
   $path = variable_get('apachesolr_path', '/solr');
  }
  else{
   $server = apachesolr_multiserver_get_server($sid);
   $host = $server[0]->apachesolr_host;
   $port = $server[0]->apachesolr_port;
   $path = $server[0]->apachesolr_path;
  }
   $reload_url = sprintf('http://%s:%s%s/admin/cores?action=RELOAD&core=%s', $host, $port, $path, $core);
   $response = file_get_contents($reload_url);
   drupal_goto('admin/settings/apachesolr/multiserver/list');
}

/**
 *
 * Allow you to do a full DIH import
 * @param $core Name of the core
 */
function apachesolr_multiserver_dih_full_import($core, $sid=NULL) {
  if (is_null($sid)) {
   $host = variable_get('apachesolr_host', 'localhost');
   $port = variable_get('apachesolr_port', '8983');
   $path = variable_get('apachesolr_path', '/solr');
  }
  else{
   $server = apachesolr_multiserver_get_server($sid);
   $host = $server[0]->apachesolr_host;
   $port = $server[0]->apachesolr_port;
   $path = $server[0]->apachesolr_path;
  }
   $reload_url = sprintf('http://%s:%s/%s/%s/dataimport?command=full-import', $host, $port, $path, $core);
   $response = file_get_contents($reload_url);
   drupal_goto('admin/settings/apachesolr/multiserver/server/list');
}

############## Solr Server Manipulation Functions

 /**
  *
  * Allows the addition/modification of Solr Servers to Drupal
  * @param $form_state
  * @param $form_type
  * @param $sid
  * @return $form
  */
  function apachesolr_multiserver_server_form($form_state, $form_type, $sid=NULL) {
    switch ($form_type) {
      case 'edit':
        if (!is_null($sid)) {
          $solr_server = array_shift(apachesolr_multiserver_get_server($sid));
        }
        $form['sid'] = array(
          '#type' => 'hidden',
          '#value' => $sid,
        );
      case 'add':
        $form['apachesolr_title'] = array(
          '#title' => t('Server Name'),
          '#type' => 'textfield',
          '#description' => t('Identifier for a solr server.'),
          '#default_value' => isset($solr_server->apachesolr_title) ? $solr_server->apachesolr_title : '',
          '#weight' => -10,
          '#required' => TRUE,
        );
       $form['apachesolr_host'] = array(
          '#type' => 'textfield',
          '#title' => t('Solr host name'),
          '#default_value' => isset($solr_server->apachesolr_host) ? $solr_server->apachesolr_host : '',
          '#description' => t('Host name of your Solr server, e.g. <code>localhost</code> or <code>example.com</code>.'),
          '#required' => TRUE,
        );
        $form['apachesolr_port'] = array(
          '#type' => 'textfield',
          '#title' => t('Solr port'),
          '#default_value' => isset($solr_server->apachesolr_port) ? $solr_server->apachesolr_port : '',
          '#description' => t('Port on which the Solr server listens. The Jetty example server is 8983, while Tomcat is 8080 by default.'),
          '#required' => TRUE,
        );
        $form['apachesolr_path'] = array(
          '#type' => 'textfield',
          '#title' => t('Solr path'),
          '#default_value' => isset($solr_server->apachesolr_path) ? $solr_server->apachesolr_path : '',
          '#description' => t('Path that identifies the Solr request handler to be used Please include leading slash.'),
          '#required' => TRUE,
        );

        break;
      default:
    }

    $form['submit_form'] = array(
      '#type' => 'submit',
      '#weight' => 10,
      '#value' => t('Save'),
    );
    return $form;
  }



  /**
   * Allows the addition/modification of Solr Servers to Drupal Submit
   * @param $form
   * @param $form_stat
   */
  function apachesolr_multiserver_server_form_submit($form, &$form_state) {
    # Before we commit lets see if we are dealing with a multicore instance
    $form_state['values']['apachesolr_is_multicore'] = apachesolr_multiserver_is_multicore($form_state['values']['apachesolr_host'], $form_state['values']['apachesolr_port'], $form_state['values']['apachesolr_path']);
    if (isset($form_state['values']['sid'])) {
      drupal_write_record('apachesolr_server', $form_state['values'], array('sid'));
    }
    else {
      drupal_write_record('apachesolr_server', $form_state['values']);
    }

    drupal_goto('admin/settings/apachesolr/multiserver/server/list');
  }

  /**
   * Allows the deletion of Solr Servers to Drupal
   * @param $form_state
   * @param $sid
   */
  function apachesolr_multiserver_server_delete(&$form_state, $sid) {
  $form['sid'] = array('#type' => 'hidden', '#value' => $sid);
  return confirm_form($form, t('Are you sure you want to delete this solr server?'), 'admin/settings/apachesolr/multicore/server/list', '', t('Delete'), t('Cancel'));
}

/**
 * Submit handler for quicktab block deletion.
 * @param $form
 * @param $form_state
 */
function apachesolr_multiserver_server_delete_submit($form, &$form_state) {
  db_query('DELETE FROM {apachesolr_server} WHERE sid = %d', $form_state['values']['sid']);
  drupal_set_message(t('The Solr Server has been removed.'));
  cache_clear_all();
  $form_state['redirect'] = 'admin/settings/apachesolr/multicore/server/list';
};


/**
 * Page callback for Apache Solr Registered Servers admin landing page.
 */
function apachesolr_multiserver_list() {
  $header = array(
    array('data' => t('Name')),
    array('data' => t('Host')),
    array('data' => t('Port')),
    array('data' => t('Path')),
    array('data' => t('Is Multicore')),
    array('data' => t('Ops'), 'colspan' => 2),
  );
  $rows = array();
  $results = apachesolr_multiserver_get_server();
  foreach ($results as $row) {
    $tablerow = array(
      array('data' => $row->apachesolr_title),
      array('data' => $row->apachesolr_host),
      array('data' => $row->apachesolr_port),
      array('data' => $row->apachesolr_path),
      array('data' => !empty($row->apachesolr_is_multicore)?"Yes (No of Cores: $row->apachesolr_is_multicore)":'No'),
      array('data' => l(t('Edit'), sprintf('admin/settings/apachesolr/multiserver/%s/edit', $row->sid))),
      array('data' => l(t('Delete'), sprintf('admin/settings/apachesolr/multiserver/%s/delete', $row->sid))),
    );
    $rows[] = $tablerow;
    if($row->apachesolr_is_multicore){
      $cores = apachesolr_multiserver_get_cores($row->apachesolr_host, $row->apachesolr_port, $row->apachesolr_path, false);
      foreach($cores as $core){
        $tablerow = array(
          array('data' => '', colspan => 3),
          array('data' => t(sprintf('%s/%s', $row->apachesolr_path, $core))),
          array('data' => ''),
          array('data' => l(t('Reload Core'), sprintf('admin/settings/apachesolr/multiserver/core/reload/%s/%s', $core, $row->sid))),
          array('data' => l(t('Full Import'), sprintf('admin/settings/apachesolr/multiserver/dih/full/%s/%s', $core, $row->sid)))
        );
      $rows[] = $tablerow;
      }
    }
  }
  if (!empty($rows)) {
    $output = theme('table', $header, $rows, array('id' => 'apachesolr_server'));
  }
  else {
    $output = '<p>'. t("You have no registered servers yet. " ) . l('Add a new server', 'admin/settings/apachesolr/multiserver/add') .'</p>';
  }
  return $output;
}

/**
 *
 * Detects if a given server is multicore
 * @param $host
 * @param $port
 * @param $path
 * @param $count Return a count of the number of cores returned else return the cores array
 */
function apachesolr_multiserver_is_multicore($host, $port, $path, $count=TRUE) {
  $url = sprintf('http://%s:%s%s/%s', $host, $port, $path, 'admin/cores');
  if ($result = @file_get_contents($url)) {
    return ($count)?count(apachesolr_multicore_parse_cores($result)):apachesolr_multicore_parse_cores($result);
  }

  return FALSE;
}

function apachesolr_server_is_active($host, $port, $path) {
  $file_exists = file_exists(dirname(__FILE__) . '/SolrPhpClient/Apache/Solr/Service.php');
  // Ensure translations don't break at install time
  $ping = FALSE;
  try {
    $solr = apachesolr_get_solr($host, $port, $path);
    $ping = @$solr->ping(variable_get('apachesolr_ping_timeout', 4));
    // If there is no $solr object, there is no server available, so don't continue.
    if (!$ping) {
      return FALSE;
    }
  }
  catch (Exception $e) {
  }
  return TRUE;
}

/**
 *
 * Grab the current avaible apache solr cores
 * @TODO Extended to get cores from different registered servers
 * @param $host
 * @param $port
 * @param $path
 * @param $with_path
 */
function apachesolr_multiserver_get_cores($host, $port, $path, $with_path=TRUE) {
  static $cores = array();
  static $mc = array();
  $url = sprintf('http://%s:%s%s/%s', $host, $port, $path, 'admin/cores');
  if (count($cores[$url])==0) {
    if ($cores[$url] = apachesolr_multiserver_is_multicore($host, $port, $path, FALSE)) {
      watchdog('Multi cores', print_r($cores,true));
      foreach ($cores[$url] as $core) {
        $mc[$url][] = sprintf('%s/%s', $path, $core);
      }
    }
    else {
      return FALSE;
    }
  }
  return ($with_path)?$mc[$url]:$cores[$url];
}

/**
 *
 * Helper function for parsing solr core xml
 * @TODO Check to see either the pecl or the standard Solr php library has this functionality
 * @param $cores_xml
 */
function apachesolr_multicore_parse_cores($cores_xml) {
  $cores = array();
  $doc = new DOMDocument();
  $doc->loadXML($cores_xml);
  $xp = new DOMXPath($doc);
  $nl = $xp->query('/response/lst[@name="status"]/lst');
  foreach ($nl as $node) {
    $cores[] = $node->getAttribute('name');
//    $mc[] = $path_default . '/' . $node->getAttribute('name');
  }
  return $cores;
}

/**
 * ahah callback
 */
function apachesolr_multiserver_ahah() {
  $form_state = array('storage' => NULL, 'rebuild' => TRUE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);

  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form['#post'] = $_POST;
  $form['#redirect'] = FALSE;
  $form['#programmed'] = FALSE;
  $form_state['post'] = $_POST;
  drupal_process_form($form_id, $form, $form_state);

  $solr_server = apachesolr_multiserver_get_server($_POST['apachesolr_server_sid']);

  if (count($solr_server) > 0) {
    $cores = array();

    if ($solr_server[0]->apachesolr_is_multicore) {
      $cores = apachesolr_multiserver_get_cores($solr_server[0]->apachesolr_host, $solr_server[0]->apachesolr_port, $solr_server[0]->apachesolr_path);
    }
    else{
      # This Server isnt multicore lets just give them the default core
      $cores = array($solr_server[0]->apachesolr_path);
    }
    $options = array('Select');
    foreach($cores as $core){
      $options[$core] = $core;
    }
    $form['apachesolr_path_default'] = array(
      '#type' => 'select',
      '#id' => 'edit-apachesolr-default-path',
      '#name' => 'apachesolr_path_default',
      '#title' => t('Solr Path'),
      '#options' => $options,
      '#default_value' => variable_get('apachesolr_path_default', 0),
      '#description' => t('A list of solr paths for the chosen server'),
      '#weight' => 0,
    );
    $form['apachesolr_host'] = array(
      '#type' => 'hidden',
      '#value' => $solr_server[0]->apachesolr_host,
    );
    $form['apachesolr_port'] = array(
      '#type' => 'hidden',
      '#value' => $solr_server[0]->apachesolr_port,
    );
    $form['apachesolr_path'] = array(
      '#type' => 'hidden',
      '#value' => $solr_server[0]->apachesolr_path,
    );
    form_set_cache($_POST['form_build_id'], $form, $form_state);
    $_f['apachesolr_path_default'] = $form['apachesolr_path_default'];
  }

  $output = drupal_render($_f) . drupal_render($form['apachesolr_path']) . drupal_render($form['apachesolr_host']) . drupal_render($form['apachesolr_port']);
  drupal_json(array(
    'status'   => TRUE,
    'data'     => theme('status_messages') .$output ,
  ));
  exit();
}

/**
 * Creates and returns a unique name for each apachesolr instance
 * Enter description here ...
 */
function apachesolr_multiserver_get_name($server, $core = NULL) {
  if ($server){
    return ($core)? "{$server->apachesolr_host}_{$server->sid}_{$core}" : "{$server->apachesolr_host}_{$server->sid}_{$server->apachesolr_path}";
  }
  return FALSE;
}
