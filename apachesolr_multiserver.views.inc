<?php
/*
 * Load files with base classes of the contained classes.
 */

/**
 * Implementation of hook_views_handlers().
 */
function apachesolr_multiserver_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'apachesolr_multiserver') . '/handlers',
    ),
    'handlers' => array(
      'apachesolr_multiserver_handler_field_generic' => array(
        'parent' => 'views_handler_field',
      ),
      'apachesolr_multiserver_handler_argument_generic' => array(
        'parent' => 'views_handler_argument',
      ),
      'apachesolr_multiserver_handler_argument_mlt_keyword' => array(
        'parent' => 'views_handler_argument',
      ),
      'apachesolr_multiserver_handler_argument_tid' => array(
        'parent' => 'views_handler_argument',
      ),
      'apachesolr_multiserver_handler_argument_uuid' => array(
        'parent' => 'views_handler_argument',
      ),
      'apachesolr_multiserver_handler_argument_nid' => array(
        'parent' => 'views_handler_argument',
      ),
      'apachesolr_multiserver_views_handler_sort' => array(
        'parent' => 'views_handler_sort',
      ),
      'apachesolr_multiserver_handler_filter_generic' => array(
        'parent' => 'views_handler_filter_string',
      )
    ),
  );
}

/**
 * Implementation of hook_view
 * Enter description here ...
 */
function apachesolr_multiserver_views_data() {
  module_load_include('inc','apachesolr_multiserver','apachesolr_multiserver.admin');
  $apachesolr_multiserver_data = $data = array();

  $registered_servers = apachesolr_multiserver_get_server();
  if (function_exists('apachesolr_view_views_data')) {
    $apachesolr_multiserver_data = $apachesolr_view_views_data();
  }

  foreach ($registered_servers as $sid => $server) {
    $mc_cores = apachesolr_multiserver_get_cores($server->apachesolr_host, $server->apachesolr_port, $server->apachesolr_path, true);

    if (!empty($mc_cores)) {
      foreach ($mc_cores as $core) {
        $solr = apachesolr_get_solr($server->apachesolr_host, $server->apachesolr_port, $core);
        $solr_name = apachesolr_multiserver_get_name($server, $core);

        $data[$solr_name] = array();
        $data[$solr_name]['table']['group'] = t("$server->apachesolr_title($core)");
        $data[$solr_name]['table']['join'] = array();
        $data[$solr_name]['table']['base'] = array(
          'query class' => 'apachesolr_views_query',
          'title' => t("Apache Solr - {$server->apachesolr_title} [{$core}]"),
          'help' => t('Searches the site with the Apache Solr search engine.'),
          'field' => 'nid',
        );

        $fields = $solr->getFields();
        foreach($fields as $field_name => $field){
          if (!isset($field->dynamicBase) || ($field->dynamicBase == '*')){
          $data[$solr_name][$field_name] = _apachesolr_multiserver_get_field_definition($field_name, $field);
          }
       }

       $data[$solr_name]['keywords'] = array(
            'title' => t('Keywords' ." ($core) "),
            'help' => t("Apache Solr Mapping for multi core specific fields"),
            'argument' => array(
            'handler' => 'apachesolr_multiserver_handler_argument_tid',
            ),
          );

       $data[$solr_name]['apachesolruuid'] = array(
        'title' => t('UUID' ." ($core) "),
            'help' => t("Apache Solr Unique identifier"),
            'argument' => array(
              'handler' => 'apachesolr_multiserver_handler_argument_uuid',
            ),
          );
        }
      }
       else {
        // We have hit a apachesolr non multicore server. If this server is already default the generic apachesolr module handles it well,
        // Deal with this only if the generic apachesolr module cannot deal with this view
        if (!apachesolr_multiserver_is_default_server($server->sid)) {
          $solr = apachesolr_get_solr($server->apachesolr_host, $server->apachesolr_port, $server->apachesolr_path);
          $solr_name = apachesolr_multiserver_get_name($server, $server->apachesolr_path);

          $data[$solr_name] = array();
          $data[$solr_name]['table']['group'] = t("$server->apachesolr_title");
          $data[$solr_name]['table']['join'] = array();

          $data[$solr_name]['table']['base'] = array(
          'query class' => 'apachesolr_views_query',
          'title' => t("Apache Solr - {$server->apachesolr_title}"),
          'help' => t('Searches the site with the Apache Solr search engine.'),
          'field' => 'nid',
        );

        $fields = $solr->getFields();
        foreach($fields as $field_name => $field){
          if (!isset($field->dynamicBase) || ($field->dynamicBase == '*')){
          $data[$solr_name][$field_name] = _apachesolr_multiserver_get_field_definition($field_name, $field);
          }
       }

       /** Special Argument Handlers **/
        $data[$solr_name]['mlt_body'] = array(
          'title' => t('More Like This Keyword data'),
          'help' => t('Do a more like this query based on the provided keyword data'),
          'argument' => array(
          'handler' => 'apachesolr_multicore_handler_argument_mlt_keyword',
            ));

        $data[$solr_name]['keywords'] = array(
            'title' => t('Keywords'),
            'help' => t("Apache Solr Mapping for multi core specific fields"),
            'argument' => array(
            'handler' => 'apachesolr_multiserver_handler_argument_tid',
            ),
          );

        $data[$solr_name]['apachesolruuid'] = array(
        'title' => t('UUID'),
            'help' => t("Apache Solr Unique identifier"),
            'argument' => array(
              'handler' => 'apachesolr_multiserver_handler_argument_uuid',
            ),
          );
      }
    }
  }
  return $data;
}

/**
 * prepares fields for view from given apachesolr document field
 * Enter description here ...
 * @param unknown_type $field_name
 * @param unknown_type $field
 */
function _apachesolr_multiserver_get_field_definition ($field_name, $field) {
  $field = array(
    'title' => t($field_name),
    'help' => t("Apache Solr Mapping for multi core specific fields"),
    'field' => array(
    'handler' => 'apachesolr_multiserver_handler_field_generic',
    'click sortable' => TRUE,
    'real field' => $field_name,
  ),
    'argument' => array(
      'handler' => 'apachesolr_multiserver_handler_field_generic',
    ),
    'sort' => array(
      'handler' => 'apachesolr_multiserver_views_handler_sort',
    ),
    'filter' => array(
        'handler' => 'apachesolr_multiserver_handler_filter_generic',
      ),
  );
  return $field;
}

