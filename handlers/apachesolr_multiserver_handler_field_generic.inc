<?php

/**
 * @file
 * defines a class to handle generic non drupal fields
 */
class apachesolr_multiserver_handler_field_generic extends views_handler_field {

    function query() {
    $this->query->add_field($this->real_field);
  }

    function render($doc) {

    if (is_array($doc->{$this->real_field})) {
        foreach ($doc->{$this->real_field} as $id => $item) {
            return serialize($doc->{$this->real_field});
        }
    }
    else {
        return $doc->{$this->real_field};
    }
  }
}