<?php

/*@file
 * Argument handler for UUID. UUID is unique document handler in sols
 * @author written by Dave Stuart (dave.stuart@gmail.com)
 * @last modified by Ravi Julapalli (rkjulapalli@gmail.com)
 *
 */

/**
 * Argument hander for apachesolr_multicore_handler_argument_tid , Also exposes a hook for other modules.
 * @TODO
 * @author rjulapalli
 *
 */
class apachesolr_multiserver_handler_argument_uuid extends views_handler_argument {
  /**
   * Override option_definition() to provide defaults
   */
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * Override options form to present specific stuffs
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

  }
  /**
   * Override query().
   */
  function query() {
    // Implementation of HOOK_<view_name>_qry_args
        foreach (module_implements($this->view->name .'_uuid_qry_args') as $module) {
                $function = $module .'_'. $this->view->name  .'_uuid_qry_args';
                $result[] = $function($this);
        }
        if ($result) {
            $this->query->set_query(implode(",", $result));
        }
    }
}